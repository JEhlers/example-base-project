package de.hsb.softw2muebase.controller;

import de.hsb.softw2muebase.model.Movie;
import de.hsb.softw2muebase.util.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MovieController {

    private final MovieRepository repo;

    public MovieController(MovieRepository repo) {
        this.repo = repo;
    }


    /*@GetMapping("/greeting/{name}")
    public String greeting(@PathVariable("name") String name) {
        if (name.length() % 2 == 0) {
            name = "wuensche ich Ihnen, " + name;
        }
        return "Guten Tag " + name;
    }*/

    @GetMapping("/movies/{idOrName}")
    public ResponseEntity get(@PathVariable("idOrName") String idOrName) {
        try {
            var movie = repo.findById(Long.parseLong(idOrName));
            return new ResponseEntity(movie.isEmpty() ? null : movie.get(), movie.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        } catch (NumberFormatException nfe) {
            var movies = repo.findByName(idOrName);
            return new ResponseEntity(movies, HttpStatus.OK);
        }
    }

    @GetMapping("/movies/")
    public ResponseEntity getAll() {
        return new ResponseEntity(repo.findAll(), HttpStatus.OK);
    }

    @PostMapping("/movies/")
    public ResponseEntity add(@RequestBody Movie movie) {
        if (movie.getName() == null)
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);

        movie.setID(repo.count());
        repo.save(movie);
        return new ResponseEntity(null, HttpStatus.CREATED);
    }
}

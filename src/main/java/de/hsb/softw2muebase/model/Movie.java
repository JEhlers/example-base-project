package de.hsb.softw2muebase.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Movie")
public class Movie {

    @Id
    private Long id;

    private final String name;

    public Movie() {
        this.name = null;
        this.id = null;
    }

    public Movie(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    public Long getID() {
        return this.id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    /*public void setName(String name) {
        this.name = name;
    }*/

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof Movie))
            return false;

        final Movie other = (Movie) obj;

        return (Objects.equals(this.id, other.id)) && (Objects.equals(this.name, other.name));
    }


    /*@Override
    public String toString() {
        return "Movie{" + "id=" + this.id + ", name=" + this.name + '}';
    }*/

}
